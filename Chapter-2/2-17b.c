/* C How to Program Sixth Edition (Deitel & Deitel).
   Solution of exercise 2.17a:

   (Printing Values with printf) Write a program that prints 
   the numbers 1 to 4 on the same line. Write the program using 
   the following methods.
		b) Using one printf statement with four conversion specifiers.

   Copyright (C) 2018, Juan Carlos Moreno (jcmhsoftware@gmail.com)

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  
   02110-1301  USA 
*/

#include <stdio.h>

int main(void)
{
	int a = 1;
	int b = 2;
	int c = 3;
	int d = 4;

	printf("%d %d %d %d\n", a, b, c, d);

	return 0;
}