/* C How to Program Sixth edition (Deitel & Deitel).
   Solution of exercise 2.19:

   (Arithmetic, Largest Value and Smallest Value) Write a program 
   that inputs three different integers from the keyboard, then 
   prints the sum, the average, the product, the smallest and the 
   largest of these numbers. Use only the single-selection form of 
   the if statement you learned in this chapter. The screen dialogue 
   should appear as follows:

   Input three different integers: 13 27 14
   Sum is 54
   Average is 18
   Product is 4914
   Smallest is 13
   Largest is 27

   Copyright (C) 2018, Juan Carlos Moreno (jcmhsoftware@gmail.com)

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  
   02110-1301  USA 
*/

#include <stdio.h>

int main(void)
{
	int a; /*First integer*/
	int b; /*Second integer*/
	int c; /*Third integer*/

	printf("Input three different integers: ");
	scanf("%d", &a);
	scanf("%d", &b);
	scanf("%d", &c);

	printf("Sum is %d\n", a + b + c);
	printf("Average is %d\n", (a + b + c)/3);
	printf("Product is %d\n", a * b * c);

	/*Print the smallest integer*/
	if (a < b)
	{
		if (a < c)
		{
			printf("Smallest is %d\n", a);
		}
	}
	if (b < a)
	{
		if (b < c)
		{
			printf("Smallest is %d\n", b);
		}
	}
	if (c < a)
	{
		if (c < b)
		{
			printf("Smallest is %d\n", c);
		}
	}

	/*Print the largest integer*/
	if (a > b)
	{
		if (a > c)
		{
			printf("Largest is %d\n", a);
		}
	}
	if (b > a)
	{
		if (b > c)
		{
			printf("Largest is %d\n", b);
		}
	}
	if (c > a)
	{
		if (c > b)
		{
			printf("Largest is %d\n", c);
		}
	}

	return 0;
}