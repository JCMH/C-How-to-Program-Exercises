/* C How to Program Sixth edition (Deitel & Deitel).
   Solution of exercise 2.18:

   Comparing Integers) Write a program that asks the user to enter 
   two integers, obtains the numbers from the user, then prints the 
   larger number followed by the words “is larger.” If the numbers 
   are equal, print the message “These numbers are equal.” Use only 
   the single-selection form of the if statement you learned in 
   this chapter.

   Copyright (C) 2018, Juan Carlos Moreno (jcmhsoftware@gmail.com)

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  
   02110-1301  USA 
*/

#include <stdio.h>

int main(void)
{
	int a;
	int b;
	
	printf("Enter the first number: ");
	scanf("%d", &a);
	printf("Enter the second number: ");
	scanf("%d", &b);

	if (a > b)
	{
		printf("%d is larger.\n", a);
	}

	if (b > a)
	{
		printf("%d is larger.\n", b);
	}

	if (a == b)
	{
		printf("These number are equal.\n", a);
	}

	return 0;
}