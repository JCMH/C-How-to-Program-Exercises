/* C How to Program Sixth edition (Deitel & Deitel).
   Solution of exercise 2.19:

   2.20 (Diameter, Circumference and Area of a Circle) Write a 
   program that reads in the radius of a circle and prints the 
   circle’s diameter, circumference and area. Use the constant value 3.14159
   for π. Perform each of these calculations inside the printf statement(s) 
   and use the conversion specifier %f. [Note: In this chapter, we have 
   discussed only integer constants and variables. In Chapter 3
   we’ll discuss floating-point numbers, i.e., values that can have 
   decimal points.]

   Input three different integers: 13 27 14
   Sum is 54
   Average is 18
   Product is 4914
   Smallest is 13
   Largest is 27

   Copyright (C) 2018, Juan Carlos Moreno (jcmhsoftware@gmail.com)

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  
   02110-1301  USA 
*/

#include <stdio.h>

int main(void)
{
	float radius;
	float pi = 3.14159;

	printf("Input the radius of the circle: ");
	scanf("%f", &radius);

	printf("Diameter of the circle is %f\n", 2 * radius);
	printf("Circumference of the circle is %f\n", 2 * pi * radius);
	printf("Area of the circle is %f\n", pi * radius * radius);

	return 0;
}