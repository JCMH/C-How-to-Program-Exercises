/* C How to Program Sixth edition (Deitel & Deitel).
   Solution of exercise 2.16:

   (Arithmetic) Write a program that asks the user 
   to enter two numbers, obtains them from the user 
   and prints their sum, product, difference, 
   quotient and remainder. 

   Copyright (C) 2018, Juan Carlos Moreno (jcmhsoftware@gmail.com)

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  
   02110-1301  USA 
*/

#include <stdio.h>

int main(void)
{
	int a;
	int b;

	printf("Enter the first number:");
	scanf("%d", &a);
	printf("Enter the second number:");
	scanf("%d", &b);

	printf("%d + %d = %d\n", a, b, a + b);
	printf("%d * %d = %d\n", a, b, a * b);
	printf("%d - %d = %d\n", a, b, a - b);
	printf("%d / %d = %d\n", a, b, a / b);
	printf("%d mod %d = %d\n", a, b, a % b);

	return 0;
}